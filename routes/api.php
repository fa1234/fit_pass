<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ReceptionController;
use App\Http\Controllers\Api\GeneratePasswordController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('reception/check', [ReceptionController::class, 'check'])->name('api.reception.check');

Route::post('password/generate', [GeneratePasswordController::class, 'generate'])->name('api.password.generate');
