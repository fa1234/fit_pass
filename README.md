
1. php artisan migrate:fresh
it will insert some fake data
2. php artisan db:seed

there are two api endpoints

1. GET: /api/reception/check
   parameter:
            'sport_facility_id' => required, it should be exist in sport_facilities table,
            'membership_card_id' => required, it should be exist in membership_cards table

2. POST: /api/password/generate
   parameter:
            'length' => required, it should be number and min length is 6,
            'strength' => required, it should be number and value needs to be only 1,2 or 3,

Second endpoint just checks GeneratePassword service (App/Services/GeneratePassword.php) functionality

There are two tests:
ReceptionCheckTest.php - which checks  GET: /api/reception/check endpoint functionality

GeneratePasswordTest.php - which checks  POST: /api/password/generate endpoint functionality (GeneratePassword service functionality)

there are additional variables in env for testing mode:
DB_DATABASE_TESTING=
DB_USERNAME_TESTING=
DB_PASSWORD_TESTING=
