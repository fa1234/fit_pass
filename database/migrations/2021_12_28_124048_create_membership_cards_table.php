<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembershipCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membership_cards', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->char('sport_facility_id', 36);
            $table->char('user_id', 36);
            //if there will be some expire date then it should be removed
            $table->unique(['sport_facility_id', 'user_id']);

            $table->timestamps();

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('RESTRICT');

            $table->foreign('sport_facility_id')
            ->references('id')
            ->on('sport_facilities')
            ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership_cards');
    }
}
