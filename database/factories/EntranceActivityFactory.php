<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class EntranceActivityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'membership_card_id' => function () {
                return \App\Models\MembershipCard::factory(1)->create()->first()->id;
            },
        ];
    }
}
