<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MembershipCardFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => function () {
                return \App\Models\User::factory(1)->create()->first()->id;
            },
            'sport_facility_id' => function () {
                return \App\Models\SportFacility::factory(1)->create()->first()->id;
            },
        ];
    }
}
