<?php

namespace Database\Seeders;

use App\Models\MembershipCard;
use App\Models\SportFacility;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(50)->create();
        \App\Models\SportFacility::factory(10)->create();

        //generate membership cards based on user
        $users = User::skip(rand(1, 20))->take(rand(5, 30))->get();

        foreach ($users as $user) {
            $sportFacility = SportFacility::all()->random(1)->first();
            \App\Models\MembershipCard::factory(1)->create(['user_id' => $user->getId(), 'sport_facility_id' => $sportFacility->getId()]);
        }

        //create today's activities for some users
        $memberships = MembershipCard::skip(rand(1, 10))->take(rand(5, 20))->get();

        foreach ($memberships as $membership) {
            \App\Models\EntranceActivity::factory(1)->create(['membership_card_id' => $membership->getId()]);
        }
    }
}
