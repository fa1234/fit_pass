<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function createUser($params = [], $count = 1)
    {
        return  \App\Models\User::factory($count)->create($params);
    }

    public function createSportFacility($params = [], $count = 1)
    {
        return  \App\Models\SportFacility::factory($count)->create($params);
    }

    public function createEntranceActivity($params = [], $count = 1)
    {
        return  \App\Models\EntranceActivity::factory($count)->create($params);
    }

    public function createMembershipCard($params = [], $count = 1)
    {
        return  \App\Models\MembershipCard::factory($count)->create($params);
    }
}
