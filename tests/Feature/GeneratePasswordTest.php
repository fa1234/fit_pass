<?php

namespace Tests\Feature;

use App\Models\EntranceActivity;
use App\Services\ErrorMessage;
use App\Services\GeneratePassword;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use function PHPUnit\Framework\assertEquals;

class GeneratePasswordTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */

    /** @test */
    public function it_should_check_that_password_length_and_strength_are_required()
    {
        $response = $this->json('POST', route('api.password.generate'));
        $response->assertStatus(422);
        $data = json_decode($response->getContent());

        $this->assertEquals(ErrorMessage::PASSWORD_LENGTH_AND_STRENGTH_IS_REQUIRED, $data);
    }

    /** @test */
    public function it_should_check_that_password_length_and_strength_has_to_be_number_type()
    {
        $response = $this->json('POST', route('api.password.generate'), [
            'length' => 'aa',
            'strength' => 3,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent());

        $this->assertEquals(ErrorMessage::PASSWORD_LENGTH_AND_STRENGTH_HAS_TO_BE_NUMBER_TYPE, $data);

        $response = $this->json('POST', route('api.password.generate'), [
            'length' => 7,
            'strength' => 'bb',
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent());

        $this->assertEquals(ErrorMessage::PASSWORD_LENGTH_AND_STRENGTH_HAS_TO_BE_NUMBER_TYPE, $data);
    }

    /** @test */
    public function it_should_check_password_allowed_min_length()
    {
        $response = $this->json('POST', route('api.password.generate'), [
            'length' => 5,
            'strength' => 3,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent());

        $this->assertEquals(ErrorMessage::PASSWORD_MIN_ALLOWED_LENGTH_ERROR, $data);
    }

    /** @test */
    public function it_should_check_password_allowed_strength_type()
    {
        $response = $this->json('POST', route('api.password.generate'), [
            'length' => 7,
            'strength' => 4,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent());

        $this->assertEquals(ErrorMessage::UNKNOWN_PASSWORD_STRENGTH, $data);
    }

    /** @test */
    public function it_should_check_that_password_length_works_correctly()
    {
        $response = $this->json('POST', route('api.password.generate'), [
            'length' => 7,
            'strength' => 1,
        ]);

        $response->assertStatus(200);
        $data = json_decode($response->getContent());

        $this->assertEquals(7, strlen($data->password));

        $response = $this->json('POST', route('api.password.generate'), [
            'length' => 15,
            'strength' => 1,
        ]);

        $response->assertStatus(200);
        $data = json_decode($response->getContent());

        $this->assertEquals(15, strlen($data->password));
    }

    /** @test */
    public function it_should_check_that_password_strength_one_works_correctly()
    {
        //case 1 - strength type 1
        $response = $this->json('POST', route('api.password.generate'), [
            'length' => 6,
            'strength' => 1,
        ]);

        $response->assertStatus(200);
        $data = json_decode($response->getContent());

        $this->assertEquals(6, strlen($data->password));

        $uppercase_characters = mb_strlen(preg_replace('![^A-Z]+!', '', $data->password));
        $lowercase_characters = mb_strlen(preg_replace('![^a-z]+!', '', $data->password));

        $this->assertTrue($uppercase_characters >= GeneratePassword::STRENGTH_ONE_MIN_UPPERCASE);
        $this->assertTrue($lowercase_characters >= GeneratePassword::STRENGTH_ONE_MIN_LOWERCASE);
    }

    /** @test */
    public function it_should_check_that_password_strength_two_works_correctly()
    {
        //case 1 - strength type 1
        $response = $this->json('POST', route('api.password.generate'), [
            'length' => 6,
            'strength' => 2,
        ]);

        $response->assertStatus(200);
        $data = json_decode($response->getContent());

        $this->assertEquals(6, strlen($data->password));

        $uppercase_characters = mb_strlen(preg_replace('![^A-Z]+!', '', $data->password));
        $lowercase_characters = mb_strlen(preg_replace('![^a-z]+!', '', $data->password));
        $numbers = mb_strlen(preg_replace('![^2-5]+!', '', $data->password));

        $this->assertTrue($uppercase_characters >= GeneratePassword::STRENGTH_ONE_MIN_UPPERCASE);
        $this->assertTrue($lowercase_characters >= GeneratePassword::STRENGTH_ONE_MIN_LOWERCASE);
        $this->assertTrue($numbers >= GeneratePassword::STRENGTH_TWO_MIN_NUMBER_LENGTH);
    }

    /** @test */
    public function it_should_check_that_password_strength_three_works_correctly()
    {
        //case 1 - strength type 1
        $response = $this->json('POST', route('api.password.generate'), [
            'length' => 6,
            'strength' => 3,
        ]);

        $response->assertStatus(200);
        $data = json_decode($response->getContent());

        $this->assertEquals(6, strlen($data->password));

        $uppercase_characters = mb_strlen(preg_replace('![^A-Z]+!', '', $data->password));
        $lowercase_characters = mb_strlen(preg_replace('![^a-z]+!', '', $data->password));
        $numbers = mb_strlen(preg_replace('![^2-5]+!', '', $data->password));
        $symbols = mb_strlen(preg_replace("/[\&%#!(){}[\]=\$]/", '', $data->password));

        $symbols = 6 - $symbols;
        $this->assertTrue($uppercase_characters >= GeneratePassword::STRENGTH_ONE_MIN_UPPERCASE);
        $this->assertTrue($lowercase_characters >= GeneratePassword::STRENGTH_ONE_MIN_LOWERCASE);
        $this->assertTrue($numbers >= GeneratePassword::STRENGTH_TWO_MIN_NUMBER_LENGTH);
        $this->assertTrue($symbols >= GeneratePassword::STRENGTH_THREE_MIN_SYMBOL);
    }
}
