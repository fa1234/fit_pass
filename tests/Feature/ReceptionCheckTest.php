<?php

namespace Tests\Feature;

use App\Models\EntranceActivity;
use App\Services\ErrorMessage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ReceptionCheckTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */

    /** @test */
    public function it_should_check_reception_check_required_fields()
    {
        $response = $this->json('get', route('api.reception.check'));

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('sport_facility_id');
        $response->assertJsonValidationErrors('membership_card_id');
    }

    /** @test */
    public function it_should_return_error_if_sport_facility_does_not_exist()
    {
        $membershipCard = $this->createMembershipCard()->first();

        $response = $this->json('get', route('api.reception.check'), [
          'sport_facility_id' => 'unknown_uuid',
          'membership_card_id' => $membershipCard->getId(),
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('sport_facility_id');
    }

    /** @test */
    public function it_should_return_error_if_membership_card_id_does_not_exists()
    {
        $sportFacility = $this->createSportFacility()->first();

        $response = $this->json('get', route('api.reception.check'), [
            'sport_facility_id' => $sportFacility->getId(),
            'membership_card_id' => 'unknown_id',
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('membership_card_id');
    }

    /** @test */
    public function if_should_return_error_if_customer_already_entered_current_day()
    {
        $sportFacility = $this->createSportFacility()->first();

        $membershipCard = $this->createMembershipCard(['sport_facility_id' => $sportFacility->getId()])->first();

        //create entrance activity record
        $this->createEntranceActivity(['membership_card_id' => $membershipCard->getId()]);

        $response = $this->json('get', route('api.reception.check'), [
            'sport_facility_id' => $sportFacility->getId(),
            'membership_card_id' => $membershipCard->getId(),
        ]);

        $data = json_decode($response->getCOntent());

        $response->assertStatus(409);
        $this->assertEquals(ErrorMessage::CUSTOMER_ALREADY_ENTERED_TODAY, $data);
    }

    /** @test */
    public function if_should_return_error_if_membership_record_does_not_exist()
    {
        $sportFacility = $this->createSportFacility()->first();
        $membershipCard = $this->createMembershipCard()->first();

        $response = $this->json('get', route('api.reception.check'), [
            'sport_facility_id' => $sportFacility->getId(),
            'membership_card_id' => $membershipCard->getId(),
        ]);

        $data = json_decode($response->getCOntent());

        $response->assertStatus(404);
        $this->assertEquals(ErrorMessage::MEMBERSHIP_NOT_FOUNT, $data);
    }

    /** @test */
    public function it_should_return_success_message_and_create_entrance_activity_record_if_correct_data_presented()
    {
        $sportFacility = $this->createSportFacility()->first();
        $membershipCard = $this->createMembershipCard(['sport_facility_id' => $sportFacility->getId()])->first();


        $response = $this->json('get', route('api.reception.check'), [
            'sport_facility_id' => $sportFacility->getId(),
            'membership_card_id' => $membershipCard->getId(),
        ]);

        $data = json_decode($response->getCOntent());

        $response->assertStatus(200);
        //check json structure
        $response->assertJsonStructure([
            'data' => [
                'status',
                'sport_facility_name',
                'first_name',
                'last_name',
            ]
          ]);

          $entranceActivity = EntranceActivity::where('membership_card_id', $membershipCard->getId())->whereDate('created_at', date("Y-m-d"))->exists();
          $this->assertTrue($entranceActivity);
    }
}
