<?php

namespace App\Http\Controllers\Api;

use Throwable;
use Illuminate\Http\Request;
use App\Services\GeneratePassword;
use App\Http\Controllers\Controller;

class GeneratePasswordController extends Controller
{
    public function generate(Request $request)
    {

        try {
            $password = GeneratePassword::generate($request->length, $request->strength);

            return response()->json(['password' => $password]);
        } catch (Throwable $e) {
            if (empty($e->getCode())) {
                $code = 500;
            } else {
                $code = $e->getCode();
            }

            return response()->json($e->getMessage(), $code);
        }
    }
}
