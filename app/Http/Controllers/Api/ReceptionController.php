<?php

namespace App\Http\Controllers\Api;

use Throwable;
use Illuminate\Http\Request;
use App\Models\MembershipCard;
use App\Services\ErrorMessage;
use App\Models\EntranceActivity;
use App\Http\Controllers\Controller;
use App\Http\Resources\ReceptionCheckResource;
use App\Http\Requests\Api\ReceptionCheckRequest;

class ReceptionController extends Controller
{
    public function check(ReceptionCheckRequest $request)
    {
        $sport_facility_id = $request->sport_facility_id;
        $membership_card_id = $request->membership_card_id;

        $membershipCard = MembershipCard::with(['currentDayEntranceActivity', 'user', 'sportFacility'])
                            ->where('id', $membership_card_id)
                            ->where('sport_facility_id', $sport_facility_id)->first();

        //check if membership exists
        if (empty($membershipCard)) {
            return response()->json("Membership Not found", 404);
        }
        //check if user entered before (today)
        if (!empty($membershipCard->currentDayEntranceActivity)) {
            return response()->json("Customer already entered today", 409);
        }

        try {
            //log new entrance
            EntranceActivity::create(['membership_card_id' => $membershipCard->getId()]);
        } catch (Throwable $e) {
            report($e);
            return response()->json(ErrorMessage::SOMETHING_WENT_WRONG, 500);
        }

        return new ReceptionCheckResource($membershipCard);
    }
}
