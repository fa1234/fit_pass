<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EntranceActivity extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'membership_card_id',
    ];
}
