<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MembershipCard extends Model
{
    use HasFactory, Uuids;


    public function entranceActivity()
    {
        return $this->hasMany(EntranceActivity::class, 'membership_card_id');
    }

    public function currentDayEntranceActivity()
    {
        return $this->hasOne(EntranceActivity::class, 'membership_card_id')->whereDate('created_at', '=', date('Y-m-d'));
    }

    public function getId()
    {
        return $this->id;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function sportFacility()
    {
        return $this->belongsTo(SportFacility::class, 'sport_facility_id');
    }
}
