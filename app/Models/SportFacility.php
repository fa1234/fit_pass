<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SportFacility extends Model
{
    use HasFactory, Uuids;

    public function getId()
    {
        return $this->id;
    }
}
