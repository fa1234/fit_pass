<?php
namespace App\Services;

use Throwable;

class GeneratePassword
{

    public const STRENGTH_ONE = 1;
    public const STRENGTH_ONE_MIN_UPPERCASE = 2;
    public const STRENGTH_ONE_MIN_LOWERCASE = 1;


    public const STRENGTH_TWO = 2;

    public const STRENGTH_TWO_MIN_NUMBER_LENGTH = 1;

    public const STRENGTH_THREE = 3;
    public const STRENGTH_THREE_MIN_SYMBOL = 1;


    public const PASSWORD_MIN_ALLOWED_LENGTH = 6;

    public const ALLOWED_STRENGTH_ARRAY = [
        self::STRENGTH_ONE,
        self::STRENGTH_TWO,
        self::STRENGTH_THREE,
    ];

    public static function generate($length, $strength)
    {
        //validate data
        //check if length and strength are presented
        if (empty($length) || empty($strength)) {
            throw new \Exception(ErrorMessage::PASSWORD_LENGTH_AND_STRENGTH_IS_REQUIRED, 422);
        }

        //check if both length and strength are numbers
        if (!is_numeric($length) || !is_numeric($strength)) {
            throw new \Exception(ErrorMessage::PASSWORD_LENGTH_AND_STRENGTH_HAS_TO_BE_NUMBER_TYPE, 422);
        }

        //check if password min length is valid
        if ($length < self::PASSWORD_MIN_ALLOWED_LENGTH) {
            throw new \Exception(ErrorMessage::PASSWORD_MIN_ALLOWED_LENGTH_ERROR, 422);
        }

        //check if strength is valid
        if (!in_array($strength, self::ALLOWED_STRENGTH_ARRAY)) {
            throw new \Exception(ErrorMessage::UNKNOWN_PASSWORD_STRENGTH, 422);
        }

        try {
            $pattern_lower = 'abcdefghjkmnpqrstuvwxyz';
            $pattern_upper = 'ABCDEFGHJKMNPQRSTUVWXYZ';
            $pattern_number = '2345';
            $pattern_symbol = '!#$%&(){}[]=';

            $patterns = [];
            $password = [];

            //strength is at least 1
            if ($strength > 0) {
                //remember patterns for future use
                $patterns[] = $pattern_upper;
                $patterns[] = $pattern_lower;

                //select two capital letters
                for ($i = 0; $i< self::STRENGTH_ONE_MIN_UPPERCASE; $i++) {
                    $password[] = $pattern_upper[rand(0, strlen((string)$pattern_upper) - 1)];
                }

                //select one lower letter
                for ($i = 0; $i< self::STRENGTH_ONE_MIN_LOWERCASE; $i++) {
                    $password[] = $pattern_lower[rand(0, strlen((string)$pattern_lower) - 1)];
                }
            }

            //strength is at least 2
            if ($strength > 1) {
                //remember patterns for future use
                $patterns[] = $pattern_number;
                //select one number
                for ($i = 0; $i< self::STRENGTH_TWO_MIN_NUMBER_LENGTH; $i++) {
                    $password[] = $pattern_number[rand(0, strlen((string)$pattern_number) - 1)];
                }
            }

            //strength is at least 3
            if ($strength > 2) {
                //remember patterns for future use
                $patterns[] = $pattern_symbol;
                //select one symbol
                for ($i = 0; $i< self::STRENGTH_THREE_MIN_SYMBOL; $i++) {
                    $password[] = $pattern_symbol[rand(0, strlen((string)$pattern_symbol) - 1)];
                }
            }

            //password contains all required char/number/symbol

            //get existing password length and calculate how much more symbol we have to generate
            $existing_password_length = count($password);
            //get difference between existing and required length
            $additional_symbols = $length - $existing_password_length;

            //generate random chars
            for ($i = 0; $i < $additional_symbols; $i++) {
                //select pattern randomly from stored patterns (which were stored based on strength)
                $pattern = $patterns[rand(0, count($patterns) - 1)];
                //select random symbol from selected pattern
                $password[] = $pattern[rand(0, strlen((string)$pattern) - 1)];
            }

            //randomize existing password sort
            shuffle($password);
            //convert array to string
            $password = implode('', $password);

            return $password;
        } catch (Throwable $e) {
            report($e);

            throw new \Exception(ErrorMessage::SOMETHING_WENT_WRONG);
        }
    }
}
