<?php
namespace App\Services;

class ErrorMessage
{

    public const MEMBERSHIP_NOT_FOUNT = 'Membership Not found';
    public const CUSTOMER_ALREADY_ENTERED_TODAY = 'Customer already entered today';
    public const UNKNOWN_PASSWORD_STRENGTH = 'Unknown password strength';
    public const PASSWORD_LENGTH_AND_STRENGTH_IS_REQUIRED = 'Password length and strength is required';
    public const PASSWORD_LENGTH_AND_STRENGTH_HAS_TO_BE_NUMBER_TYPE = 'Password length and strength has to be number type';
    public const PASSWORD_MIN_ALLOWED_LENGTH_ERROR = 'Password Length must be at least ' . GeneratePassword::PASSWORD_MIN_ALLOWED_LENGTH;
    public const SOMETHING_WENT_WRONG = 'Something Went Wrong';
}
